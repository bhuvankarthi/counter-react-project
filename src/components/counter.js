import { Component } from "react";
import "./counter.css";
class Counter extends Component {
  constructor() {
    super();
    this.state = {
      value: 0,
      add: 1,
      max: 200,
      IsClicked: false,
      isMaximum: false,
    };
  }

  //increment functionality

  increment = () => {
    let val = this.state.value + Number(this.state.add);
    if (val <= this.state.max) {
      this.setState({
        value: val,
      });
    } else {
      this.setState({
        value: this.state.max,
        isMaximum: true,
        IsClicked: false,
      });
    }
  };

  //decrement functionality

  decrement = () => {
    let val = this.state.value - Number(this.state.add);
    if (val > this.state.max * -1) {
      this.setState({
        value: val,

        isMaximum: false,
      });
    } else {
      this.setState({
        value: this.state.max * -1,
        isMaximum: true,
      });
    }
  };

  //reset functionality

  reset = () => {
    this.setState({
      value: 0,
      add: 1,
      max: 200,
      IsClicked: false,
    });
  };

  //handling the click events

  changeHandle = (event) => {
    if (!this.state.IsClicked) {
      this.setState({
        add: event.target.textContent,
        IsClicked: true,
      });
      return event.target.textContent;
    } else {
      this.setState({
        max: event.target.textContent,
      });
      return event.target.textContent;
    }
  };

  // max value reached message

  finalMessage = () => {
    if (this.state.isMaximum) {
      return <div className="alert-message">Max Value Reached</div>;
    } else {
      return;
    }
  };

  render() {
    return (
      <>
        <div className="output">{this.state.value}</div>
        <header className="header">
          <h2>Value</h2>
          <h2>Max</h2>
        </header>
        <this.finalMessage />
        <section className="mid-section">
          <div className="values-buttons">
            <button onClick={this.changeHandle}>5</button>
            <button onClick={this.changeHandle}>10</button>
            <button onClick={this.changeHandle}>20</button>
            <button onClick={this.changeHandle}>50</button>
            <button onClick={this.changeHandle}>100</button>
            <button onClick={this.changeHandle}>200</button>
          </div>

          <div className="state-buttons">
            <button onClick={this.increment}>Increment</button>
            <button onClick={this.decrement}>Decrement</button>
            <button onClick={this.reset}>Reset</button>
          </div>
        </section>
      </>
    );
  }
}

export default Counter;
